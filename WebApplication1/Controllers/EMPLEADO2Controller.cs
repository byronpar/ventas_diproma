﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EMPLEADO2Controller : Controller
    {
        private DataBase db = new DataBase();

        // GET: EMPLEADO2
        public async Task<ActionResult> Index()
        {
            var eMPLEADO = db.EMPLEADO.Include(e => e.EMPLEADO2).Include(e => e.TIPO_EMPLEADO).Include(e => e.USUARIO).Include(e => e.VEHICULO);
            return View(await eMPLEADO.ToListAsync());
        }

        // GET: EMPLEADO2/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLEADO eMPLEADO = await db.EMPLEADO.FindAsync(id);
            if (eMPLEADO == null)
            {
                return HttpNotFound();
            }
            return View(eMPLEADO);
        }

        // GET: EMPLEADO2/Create
        public ActionResult Create()
        {
            ViewBag.idJefe = new SelectList(db.EMPLEADO, "nit", "nombre");
            ViewBag.idTipoEmpleado = new SelectList(db.TIPO_EMPLEADO, "id", "nombre");
            ViewBag.idUsuario = new SelectList(db.USUARIO, "usuario1", "pass");
            ViewBag.idVehiculo = new SelectList(db.VEHICULO, "placa", "marca");
            return View();
        }

        // POST: EMPLEADO2/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "nit,nombre,apellido,fechaNacimiento,direccion,telefono,celular,email,idTipoEmpleado,idJefe,idUsuario,idVehiculo")] EMPLEADO eMPLEADO)
        {
            if (ModelState.IsValid)
            {
                db.EMPLEADO.Add(eMPLEADO);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idJefe = new SelectList(db.EMPLEADO, "nit", "nombre", eMPLEADO.idJefe);
            ViewBag.idTipoEmpleado = new SelectList(db.TIPO_EMPLEADO, "id", "nombre", eMPLEADO.idTipoEmpleado);
            ViewBag.idUsuario = new SelectList(db.USUARIO, "usuario1", "pass", eMPLEADO.idUsuario);
            ViewBag.idVehiculo = new SelectList(db.VEHICULO, "placa", "marca", eMPLEADO.idVehiculo);
            return View(eMPLEADO);
        }

        // GET: EMPLEADO2/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLEADO eMPLEADO = await db.EMPLEADO.FindAsync(id);
            if (eMPLEADO == null)
            {
                return HttpNotFound();
            }
            ViewBag.idJefe = new SelectList(db.EMPLEADO, "nit", "nombre", eMPLEADO.idJefe);
            ViewBag.idTipoEmpleado = new SelectList(db.TIPO_EMPLEADO, "id", "nombre", eMPLEADO.idTipoEmpleado);
            ViewBag.idUsuario = new SelectList(db.USUARIO, "usuario1", "pass", eMPLEADO.idUsuario);
            ViewBag.idVehiculo = new SelectList(db.VEHICULO, "placa", "marca", eMPLEADO.idVehiculo);
            return View(eMPLEADO);
        }

        // POST: EMPLEADO2/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "nit,nombre,apellido,fechaNacimiento,direccion,telefono,celular,email,idTipoEmpleado,idJefe,idUsuario,idVehiculo")] EMPLEADO eMPLEADO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eMPLEADO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idJefe = new SelectList(db.EMPLEADO, "nit", "nombre", eMPLEADO.idJefe);
            ViewBag.idTipoEmpleado = new SelectList(db.TIPO_EMPLEADO, "id", "nombre", eMPLEADO.idTipoEmpleado);
            ViewBag.idUsuario = new SelectList(db.USUARIO, "usuario1", "pass", eMPLEADO.idUsuario);
            ViewBag.idVehiculo = new SelectList(db.VEHICULO, "placa", "marca", eMPLEADO.idVehiculo);
            return View(eMPLEADO);
        }

        // GET: EMPLEADO2/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EMPLEADO eMPLEADO = await db.EMPLEADO.FindAsync(id);
            if (eMPLEADO == null)
            {
                return HttpNotFound();
            }
            return View(eMPLEADO);
        }

        // POST: EMPLEADO2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            EMPLEADO eMPLEADO = await db.EMPLEADO.FindAsync(id);
            db.EMPLEADO.Remove(eMPLEADO);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
