﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ORDEN_DE_COMPRAController : Controller
    {
        private DataBase db = new DataBase();

        // GET: ORDEN_DE_COMPRA
        public async Task<ActionResult> Index()
        {
            var oRDEN_DE_COMPRA = db.ORDEN_DE_COMPRA.Include(o => o.CLIENTE).Include(o => o.EMPLEADO);
            return View(await oRDEN_DE_COMPRA.ToListAsync());
        }

        // GET: ORDEN_DE_COMPRA/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDEN_DE_COMPRA oRDEN_DE_COMPRA = await db.ORDEN_DE_COMPRA.FindAsync(id);
            if (oRDEN_DE_COMPRA == null)
            {
                return HttpNotFound();
            }
            return View(oRDEN_DE_COMPRA);
        }

        // GET: ORDEN_DE_COMPRA/Create
        public ActionResult Create()
        {
            ViewBag.idCliente = new SelectList(db.CLIENTE, "nit", "nombre");
            ViewBag.idEmpleado = new SelectList(db.EMPLEADO, "nit", "nombre");
            return View();
        }

        // POST: ORDEN_DE_COMPRA/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,fecha,estado,idCliente,idEmpleado")] ORDEN_DE_COMPRA oRDEN_DE_COMPRA)
        {
            if (ModelState.IsValid)
            {
                db.ORDEN_DE_COMPRA.Add(oRDEN_DE_COMPRA);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idCliente = new SelectList(db.CLIENTE, "nit", "nombre", oRDEN_DE_COMPRA.idCliente);
            ViewBag.idEmpleado = new SelectList(db.EMPLEADO, "nit", "nombre", oRDEN_DE_COMPRA.idEmpleado);
            return View(oRDEN_DE_COMPRA);
        }

        // GET: ORDEN_DE_COMPRA/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDEN_DE_COMPRA oRDEN_DE_COMPRA = await db.ORDEN_DE_COMPRA.FindAsync(id);
            if (oRDEN_DE_COMPRA == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCliente = new SelectList(db.CLIENTE, "nit", "nombre", oRDEN_DE_COMPRA.idCliente);
            ViewBag.idEmpleado = new SelectList(db.EMPLEADO, "nit", "nombre", oRDEN_DE_COMPRA.idEmpleado);
            return View(oRDEN_DE_COMPRA);
        }

        // POST: ORDEN_DE_COMPRA/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,fecha,estado,idCliente,idEmpleado")] ORDEN_DE_COMPRA oRDEN_DE_COMPRA)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oRDEN_DE_COMPRA).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idCliente = new SelectList(db.CLIENTE, "nit", "nombre", oRDEN_DE_COMPRA.idCliente);
            ViewBag.idEmpleado = new SelectList(db.EMPLEADO, "nit", "nombre", oRDEN_DE_COMPRA.idEmpleado);
            return View(oRDEN_DE_COMPRA);
        }

        // GET: ORDEN_DE_COMPRA/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ORDEN_DE_COMPRA oRDEN_DE_COMPRA = await db.ORDEN_DE_COMPRA.FindAsync(id);
            if (oRDEN_DE_COMPRA == null)
            {
                return HttpNotFound();
            }
            return View(oRDEN_DE_COMPRA);
        }

        // POST: ORDEN_DE_COMPRA/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ORDEN_DE_COMPRA oRDEN_DE_COMPRA = await db.ORDEN_DE_COMPRA.FindAsync(id);
            db.ORDEN_DE_COMPRA.Remove(oRDEN_DE_COMPRA);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
