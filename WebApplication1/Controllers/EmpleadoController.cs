﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class EmpleadoController : Controller
    {
        private DataBase datos = new DataBase();
        
        
        public async Task<ActionResult> Index(int? nit)
        {
            EMPLEADO empleado = await datos.EMPLEADO.FindAsync(nit);
            ViewBag.nombreJefe = nombreJefe(empleado);
            ViewBag.apellidoJefe = apellidoJefe(empleado);
            return View(empleado);
        }
        
        
        public string nombreJefe(EMPLEADO empleado) {
            
                EMPLEADO jefe = datos.EMPLEADO.FirstOrDefault(e => e.nit == empleado.idJefe);
                return jefe.nombre;  
        }

        
        public string apellidoJefe(EMPLEADO empleado)
        {
                EMPLEADO jefe = datos.EMPLEADO.FirstOrDefault(e => e.nit == empleado.idJefe);
                return jefe.apellido;
        }
        public async Task<ActionResult> Main2(int? nit)
        {
            EMPLEADO empleado = await datos.EMPLEADO.FindAsync(nit);
            return View(empleado);
        }

        
        public async Task<ActionResult> GenerarOrden(int? nit)
        {
            EMPLEADO empleado = await datos.EMPLEADO.FindAsync(nit);
            return View(empleado);
        }

    }
}