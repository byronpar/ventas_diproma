﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Services.Protocols;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private DataBase datos = new DataBase();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string message = "")
        {
            
            ViewBag.Message = message;
            return View();
        }

        //[HttpPost]
        public ActionResult Loguearse(string usuarioS, string password)
        {
            if (!string.IsNullOrEmpty(usuarioS) && !string.IsNullOrEmpty(password))
            {
                try
                {
                    
                    int usuario = Int32.Parse(usuarioS);
                    var user = datos.USUARIO.FirstOrDefault(e => e.usuario1 == usuario && e.pass == password);
                    //si el usuario es diferente de null
                    if (user != null)
                    {


                        EMPLEADO empleado = datos.EMPLEADO.FirstOrDefault(e => e.idUsuario == user.usuario1);
                        empleado.TIPO_EMPLEADO = datos.TIPO_EMPLEADO.FirstOrDefault(e => e.id == empleado.idTipoEmpleado);
                        empleado.EMPLEADO2 = datos.EMPLEADO.FirstOrDefault(e=> e.nit == empleado.idJefe);
                        
                        //encontre un usuario con los datos
                        if (empleado.TIPO_EMPLEADO.nombre == "administrador")
                        {               //significa que es el Admin quien entrara a sesion
                            FormsAuthentication.SetAuthCookie(user.usuario1.ToString(), true);
                            return RedirectToAction("Index", "Empleado", empleado);
                        }
                        else if (empleado.TIPO_EMPLEADO.nombre == "vendedor")   //significa que es el Vendedor quien entrara a sesion
                        {
                            
                            FormsAuthentication.SetAuthCookie(user.usuario1.ToString(), true);
                            return RedirectToAction("Index", "Empleado", empleado);
                        }
                        else if (empleado.TIPO_EMPLEADO.nombre == "supervisor")  //significa que es un supervisor
                        {
                            FormsAuthentication.SetAuthCookie(user.usuario1.ToString(), true);
                            return RedirectToAction("Index", "Supervisor", empleado);
                        }
                        else
                        {                                          //significa que es Gerente
                            FormsAuthentication.SetAuthCookie(user.usuario1.ToString(), true);
                            return RedirectToAction("Index", "Gerente", empleado);
                        }

                    }
                    else
                    {
                        return RedirectToAction("Login", new { message = "Usuario no registrado" });
                    }
                }
#pragma warning disable CS0168 // La variable está declarada pero nunca se usa
                catch (Exception e)
#pragma warning restore CS0168 // La variable está declarada pero nunca se usa
                {
                    return RedirectToAction("Login", new { message = "Error en datos" });
               }
                
              
            }
            else
            {
                return RedirectToAction("Login", new { message = "Llena los campos para poder Iniciar Sesion" });
            }

        }

        [Authorize]
        public ActionResult Logout() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

    }
}